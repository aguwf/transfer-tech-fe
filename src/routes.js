// Thực hiện điều hướng ở files này
import React from 'react'
import { Route, Switch, BrowserRouter } from 'react-router-dom'
import * as Page from './pages'

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path={'/'} component={Page.HomePage} />
      {/* <Route exact path={'/login'} component={Page.LoginPage} />
      <Route exact path={'/404'} component={Page.NotFoundPage} />
      <Redirect to='/404' /> */}
    </Switch>
  </BrowserRouter>
)

export default Routes
