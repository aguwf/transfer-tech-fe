import { all } from 'redux-saga/effects';
import {TodoSaga} from "./TodoSagas";

export default function* rootSaga() {
  yield all([
      ...TodoSaga
  ]);
}