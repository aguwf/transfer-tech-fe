import React, { Component } from 'react';
import TodoContainers from "../containers/TodoContainers";

class HomePage extends Component {
  render() {
    return (
      <div className="HomePage">
          <TodoContainers/>
      </div>
    );
  }
}

export default HomePage;
