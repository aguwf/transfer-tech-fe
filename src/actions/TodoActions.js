import * as types from '../constants'

export const uploadData = (payload) => {
  return {
    type: types.UPLOAD_IMAGE_REQUEST,
    payload
  }
}
