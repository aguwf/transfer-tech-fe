//DOMAIN
export const DOMAIN = 'http://localhost:3001'

//HEADER
export const HTTP_HEADER_JSON = { 'Content-Type': 'application/json' }

//Limit
export const LIMIT = 10
//Method
export const HTTP_READ = 'GET'
export const HTTP_ADD = 'POST'
export const HTTP_UPDATE = 'PUT'
export const HTTP_DELETE = 'DELETE'

export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'

export const UPLOAD_IMAGE_REQUEST = 'UPLOAD_IMAGE_REQUEST'
export const UPLOAD_IMAGE_SUCCESS = 'UPLOAD_IMAGE_SUCCESS'
export const UPLOAD_IMAGE_FAILURE = 'UPLOAD_IMAGE_FAILURE'
