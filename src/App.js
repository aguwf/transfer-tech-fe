import './App.css'
import Routes from './routes'

function App() {
  return (
    <div className='App'>
      {/* Gọi điều hướng vào đây */}
      <Routes />
    </div>
  )
}

export default App
