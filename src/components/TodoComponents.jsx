import React, { Component } from 'react'
import { Editor } from '@tinymce/tinymce-react'
// import {CKEditor} from '@ckeditor/ckeditor5-react';
// import Editor from 'ckeditor5-custom-build/build/ckeditor';
// import MyUploadAdapter from './UploadComponents';
import marked from 'marked'

// const custom_config = {
//   extraPlugins: [MyCustomUploadAdapterPlugin],
//   toolbar: {
//     items: [
//       'CKFinder',
//       'heading',
//       '|',
//       'fontFamily',
//       'fontSize',
//       'fontColor',
//       'fontBackgroundColor',
//       'highlight',
//       'bold',
//       'italic',
//       'underline',
//       'strikethrough',
//       'link',
//       'bulletedList',
//       'numberedList',
//       'removeFormat',
//       '|',
//       'alignment',
//       'outdent',
//       'indent',
//       'pageBreak',
//       'restrictedEditingException',
//       '|',
//       'imageUpload',
//       'imageInsert',
//       'blockQuote',
//       'insertTable',
//       'mediaEmbed',
//       'undo',
//       'redo',
//       'specialCharacters',
//     ],
//   },
//   table: {
//     contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells'],
//   },
// };

function preview(value) {
  return { __html: marked(value) }
}

// function MyCustomUploadAdapterPlugin(editor) {
//   editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
//     return new MyUploadAdapter(loader);
//   };
// }

class TodoComponents extends Component {
  state = {
    objAdd: {
      title: ''
    },
    objUpdate: {
      id: null,
      title: ''
    },
    editorHidden: true,
    imageState: null,
    imgSrc: 'https://via.placeholder.com/300',
    selectedFile: null
  }
  handleEditorChange = (content, editor) => {
    this.setState({ objUpdate: { ...this.state.objUpdate, title: content } })
  }

  onChange = (editorState) => this.setState({ editorState })

  loadFile = function (event) {
    const image = document.getElementById('output')
    image.src = URL.createObjectURL(event.target.files[0])
  }

  render() {
    return (
      <div>
        <div>
          <hr />
          <img
            src={this.state.imgSrc}
            height='auto'
            width='300'
            alt={'upload'}
          />
          <hr />
          <input
            name='image'
            id='file'
            accept={'image/*'}
            type={'file'}
            ref={'file'}
            onChange={(event) => {
              if (!event.target.files[0]) {
                const file = this.state.selectedFile
                this.setState({
                  ...this.state,
                  imgSrc: URL.createObjectURL(file)
                })
                console.log('No file', file)
                // const reader = new FileReader()
                // reader.readAsDataURL(file)
                // reader.onloadend = () => {
                //   this.setState({
                //     ...this.state,
                //     imgSrc: [reader.result]
                //   })
                // }
              } else {
                const file = event.target.files[0]
                // console.log(URL.createObjectURL(file))
                // this.setState({
                //   ...this.state,
                //   imgSrc: URL.createObjectURL(file)
                // })
                const reader = new FileReader()
                reader.readAsDataURL(file)
                reader.onloadend = () => {
                  this.setState({
                    ...this.state,
                    imgSrc: [reader.result]
                  })
                }
                this.setState({
                  ...this.state,
                  selectedFile: event.target.files[0]
                })
              }
            }}
          />
          <hr />
          <button
            onClick={() => {
              this.props.uploadImage({
                image: this.state.selectedFile,
                film: {
                  FilmName: 'Iron Man',
                  Director: 'John Favouver',
                  Age: 15,
                  Genre: ['action, adventure, fantastic'],
                  Score: ['IMDB: 9.8', 'RottenTomato: 70%']
                }
              })
            }}
          >
            Upload
          </button>
        </div>
        <div>
          <Editor
            value={this.state.objUpdate.title}
            apiKey={'685y88qo816uxspgitkx9i6a4nmhs1qabzu0brhu0p6oyw4s'}
            init={{
              selector: 'textarea',
              height: 465,
              menubar: false,
              plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount',
                'image'
              ],
              images_upload_handler: function (blobInfo, success, failure) {
                var xhr, formData, url

                xhr = new XMLHttpRequest()
                xhr.withCredentials = false
                xhr.open('POST', 'http://localhost:3010/image')

                xhr.onload = function () {
                  var json

                  if (xhr.status !== 200) {
                    failure('HTTP Error: ' + xhr.status)
                    return
                  }

                  json = JSON.parse(xhr.responseText)

                  if (!json || typeof json.Url != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText)
                    return
                  }

                  success(json.Url)
                }
                formData = new FormData()
                formData.append('upload', blobInfo.blob(), blobInfo.filename())

                xhr.send(formData)
              },
              toolbar: [
                'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify |bullist numlist outdent indent | removeformat|image | help'
              ]
            }}
            onEditorChange={this.handleEditorChange}
            outputFormat={'html'}
          />
        </div>
        {/* <div className='App'>
          <h2>Using CKEditor 5 build in React</h2>
          <CKEditor
            required
            editor={Editor}
            config={custom_config}
            data={'Lorem'}
            // onChange={(event, editor) => {
            //   const data = editor.getData();
            //   onChange(data);
            // }}
          />
        </div>*/}
      </div>
    )
  }
}

export default TodoComponents
